﻿using BusinessLayer.BLs;
using BusinessLayer.IBLs;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticoClase1
{
    public class Commands
    {
        IBL_Personas _personasBL;

        public Commands(IBL_Personas personasBL)
        {
            _personasBL = personasBL;
        }

        public void AddPersona()
        {
            // Pedimos los datos de la pesona.
            Persona persona = new Persona();
            Console.WriteLine("Ingrese el documento de la persona: ");
            persona.Documento = Console.ReadLine();
            Console.WriteLine("Ingrese el nombre de la persona: ");
            persona.Nombre = Console.ReadLine();
            Console.WriteLine("Ingrese el apellido de la persona: ");
            persona.apellido = Console.ReadLine();
            Console.WriteLine("Ingrese el telefono de la persona: ");
            persona.telefono = Console.ReadLine();
            Console.WriteLine("Ingrese la direccion de la persona: ");
            persona.direccion = Console.ReadLine();
            Console.WriteLine("Ingrese el nacimiento de la persona: ");
            persona.nacimiento = Console.ReadLine();

            _personasBL.Insert(persona);
            Console.WriteLine("Persona insertada correctamente!" + persona.Documento);

            _personasBL.Get(persona.Documento).Print();
        }

        public void ListPersonas()
        {
            List<Persona> personas = _personasBL.Get();

            Console.WriteLine("Listado de personas:");
            Console.WriteLine("| Documento | Nombre | Apellido | Telefono | Direccion | Nacimiento ");

            foreach (Persona persona in personas)
            {
                persona.PrintTable();
            }
        }

        public void RemovePersona()
        {
            try
            {
                Console.WriteLine("Ingrese el documento de la persona a eliminar: ");
                string documento = Console.ReadLine();

                _personasBL.Delete(documento);

                Console.WriteLine("Persona eliminada correctamente.");
            } catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
    }
}
