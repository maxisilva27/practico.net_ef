﻿using BusinessLayer.IBLs;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticoClase1
{
    public class CommandsVechile
    {
        IBL_Vehicle _vehiclesBL;

        public CommandsVechile(IBL_Vehicle personasBL)
        {
            _vehiclesBL = personasBL;
        }

        public void AddVehicle()
        {
            try
            {
                // Pedimos los datos del vehiculo.
                Vehiculo veiculo = new Vehiculo();
                Console.WriteLine("Ingrese la matricula del vehiculo: ");
                veiculo.matricula = Console.ReadLine();
                Console.WriteLine("Ingrese el modelo del vehiculo: ");
                veiculo.modelo = Console.ReadLine();
                Console.WriteLine("Ingrese la marca del vehiculo: ");
                veiculo.marca = Console.ReadLine();
                Console.WriteLine("Ingrese el documento del titular:");
                veiculo.persona = new Persona();
                veiculo.persona.Documento = Console.ReadLine();

                _vehiclesBL.Insert(veiculo);
                Console.WriteLine("Vehiculo insertado correctamente!");

                _vehiclesBL.Get(veiculo.matricula).Print();
            } catch(Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public void ListVehiculos()
        {
            List<Vehiculo> vehicles = _vehiclesBL.Get();

            Console.WriteLine("Listado de vehiculos:");
            Console.WriteLine("| Marca | Matricula | Modelo | Persona ");

            foreach (Vehiculo vehicle in vehicles)
            {
                vehicle.PrintTable();
            }
        }

        public void RemoveVehiculo()
        {
            try
            {
                Console.WriteLine("Ingrese la matricula del vehiculo a eliminar: ");
                string matricula = Console.ReadLine();

                _vehiclesBL.Delete(matricula);

                Console.WriteLine("Vehiculo eliminado correctamente");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
    }
}
