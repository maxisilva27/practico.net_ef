﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class Vehiculo
    {
        public string matricula = "";
        public string marca = "";
        public string modelo = "";
        public Persona persona = null;


        public void Print()
        {
            Console.WriteLine("---- Vehiculo -----");
            Console.WriteLine("Marca: " + marca);
            Console.WriteLine("Matricula: " + matricula);
            Console.WriteLine("Modelo: " + modelo);
            if (persona != null)
            {
                Console.WriteLine("Persona: " + persona.Documento + " - " + persona.Nombre);
            }
        }

        public void PrintTable()
        {
            Console.WriteLine("| " + marca + " | " + matricula + " |" + modelo + " | " + persona.Documento  + "-" + persona.Nombre + " |");
        }
    }
}