﻿namespace Shared
{
    public class Persona
    {
        public string Nombre { get; set; } = "-- Sin Nombre --";

        public string apellido = "";
        public string direccion = "";
        public string nacimiento = "";
        public string telefono = "";

        public string Documento = "";
        
        public void Print()
        {
            Console.WriteLine("---- Persona -----");
            Console.WriteLine("Nombre: " + Nombre);
            Console.WriteLine("Documento: " + Documento);
            Console.WriteLine("Apellido: " + apellido);
            Console.WriteLine("Nacimiento: " + nacimiento);
            Console.WriteLine("Direccion: " + direccion);
            Console.WriteLine("Telefono: " + telefono);
        }

        public void PrintTable()
        {
            Console.WriteLine("| " + Documento + " | " + Nombre + " |" + apellido + " | " + telefono + " |" + "| " + direccion + " | " + nacimiento + " |");
        }
    }
}