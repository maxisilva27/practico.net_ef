﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_Vehiculo
    {
        List<Vehiculo> Get();

        Vehiculo Get(string matricula);

        void Insert(Vehiculo vehicle);

        void Update(Vehiculo vehicle);

        void Delete(string matricula);
    }
}
