﻿using DataAccessLayer.EFModels;
using DataAccessLayer.IDALs;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Vehiculos_EF : IDAL_Vehiculo
    {
        private DBContextCore _dbContext;

        public DAL_Vehiculos_EF(DBContextCore dbContext)
        {
            _dbContext = dbContext;
        }

        public void Delete(string matricula)
        {
            Vehiculos vehiculo = _dbContext.Vehiculos.FirstOrDefault(x => x.Matricula == matricula);
            if (vehiculo == null)
            {
                throw new Exception("El vehiculo que intentas eliminar no existe");
            }
            else
            {
                _dbContext.Vehiculos.Remove(vehiculo);
                _dbContext.SaveChanges();
            }
        }

        public List<Vehiculo> Get()
        {
            try
            {
                List<Vehiculo> lista = _dbContext.Vehiculos
                             .Select(p => new Vehiculo { matricula = p.Matricula, marca = p.Marca, modelo = p.Modelo, persona = new Persona { Nombre = p.persona.Nombres, Documento = p.persona.Documento } })
                             .ToList();
                return lista;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error listando personas");
                Console.WriteLine(ex.Message);
                return new List<Vehiculo>();
            }
        }

        public Vehiculo Get(string matricula)
        {
            Vehiculos p = _dbContext.Vehiculos.FirstOrDefault(x => x.Matricula == matricula);
            Vehiculo vehicle = new Vehiculo();
            vehicle.matricula = p.Matricula;
            vehicle.marca = p.Marca;
            vehicle.modelo = p.Modelo;
            Persona pers = new Persona();
            pers.Documento = p.persona.Documento;
            pers.Nombre = p.persona.Nombres;
            vehicle.persona = pers;
            return vehicle;
        }

        public void Insert(Vehiculo vehicle) {
                Vehiculos vehicleNew = new Vehiculos();
                vehicleNew.Marca = vehicle.marca;
                vehicleNew.Modelo = vehicle.modelo;
                vehicleNew.Matricula = vehicle.matricula;
                Personas person = _dbContext.Personas.FirstOrDefault(x => x.Documento == vehicle.persona.Documento);
                if (person == null)
                {
                    throw new Exception("El titular del vehiculo es invalido");
                }
                vehicleNew.persona = person;
                _dbContext.Vehiculos.Add(vehicleNew);
                _dbContext.SaveChanges();
        }

        public void Update(Vehiculo vehicle)
        {
            throw new NotImplementedException();
        }
    }
}


