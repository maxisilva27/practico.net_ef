﻿using DataAccessLayer.EFModels;
using DataAccessLayer.IDALs;
using Microsoft.Data.SqlClient;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Personas_EF : IDAL_Personas
    {
        private DBContextCore _dbContext;

        public DAL_Personas_EF(DBContextCore dbContext)
        {
            _dbContext = dbContext;
        }

        public void Delete(string documento)
        {
            Personas persona = _dbContext.Personas.FirstOrDefault(x => x.Documento == documento);
            if (persona == null)
            {
                throw new Exception("La persona que intentas eliminar no existe");
            } else
            {
                _dbContext.Personas.Remove(persona);
                _dbContext.SaveChanges();
            }
        }

        public List<Persona> Get()
        {
            return _dbContext.Personas
                             .Select(p => new Persona { Documento = p.Documento, Nombre = p.Nombres, apellido = p.Apellidos, direccion = p.Direccion, nacimiento = p.FechaNacimiento.ToString(), telefono = p.Telefono })
                             .ToList();
        }

        public Persona Get(string documento)
        {
            Personas p = _dbContext.Personas.FirstOrDefault(x => x.Documento == documento);
            Persona pNueva = new Persona();
            pNueva.telefono = p.Telefono;
            pNueva.direccion = p.Direccion;
            pNueva.Nombre = p.Nombres;
            pNueva.apellido = p.Apellidos;
            pNueva.nacimiento = p.FechaNacimiento.ToString();
            pNueva.Documento = p.Documento;
            return pNueva;
        }

        public void Insert(Persona persona)
        {
            try
            {
                Personas personaNueva = new Personas();
                personaNueva.Apellidos = persona.apellido;
                personaNueva.Telefono = persona.telefono;
                personaNueva.Direccion = persona.direccion;
                personaNueva.FechaNacimiento = DateTime.Parse(persona.nacimiento);
                personaNueva.Nombres = persona.Nombre;
                personaNueva.Documento = persona.Documento;
                _dbContext.Personas.Add(personaNueva);
                _dbContext.SaveChanges();

            } catch(Exception ex)
            {
                Console.WriteLine("Error al insertar persona");
            }
        }

        public void Update(Persona persona)
        {
            throw new NotImplementedException();
        }
    }
}
