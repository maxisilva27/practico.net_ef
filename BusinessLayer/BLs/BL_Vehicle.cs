﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class Bl_Vehicle : IBL_Vehicle
    {
        private IDAL_Vehiculo _vehiculos;

        public Bl_Vehicle(IDAL_Vehiculo vehiculos)
        {
            _vehiculos = vehiculos;
        }

        public List<Vehiculo> Get()
        {
            return _vehiculos.Get();
        }

        public Vehiculo Get(string documento)
        {
            return _vehiculos.Get(documento);
        }

        public void Insert(Vehiculo vehicle)
        {
            _vehiculos.Insert(vehicle);
        }

        public void Update(Vehiculo vehicle)
        {
            _vehiculos.Update(vehicle);
        }

        public void Delete(string matricula)
        {
            _vehiculos.Delete(matricula);
        }
    }
}
